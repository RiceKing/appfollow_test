import React, { Component } from 'react';
import { Card, Header, Modal, Button, Segment } from 'semantic-ui-react'
import CollectionApp from 'components/collection-app';

export default class CollectionApps extends Component {
  render() {
    return (
      <div>
        <Segment clearing>
          <Header as='h3' floated='left'>Новое и интересное</Header>
          <Modal trigger={<Button floated='right'>См. все</Button>}>
            <Modal.Header>Profile Picture</Modal.Header>
            <Modal.Content>
              <Modal.Description>
                <Header>Modal Header</Header>
                <p>This is an example of expanded content that will cause the modals dimmer to scroll</p>
              </Modal.Description>
            </Modal.Content>
          </Modal>
        </Segment>
        <Card.Group className='cards--row'>
          <CollectionApp />
          <CollectionApp />
          <CollectionApp />
          <CollectionApp />
          <CollectionApp />
          <CollectionApp />
          <CollectionApp />
          <CollectionApp />
          <CollectionApp />
          <CollectionApp />
          <CollectionApp />
          <CollectionApp />
          <CollectionApp />
          <CollectionApp />
        </Card.Group>
      </div>
    )
  }
}
