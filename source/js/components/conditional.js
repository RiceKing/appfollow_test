import React, { Component } from 'react';
import { Button } from 'semantic-ui-react';

export default class Conditional extends Component {
  render() {
    return (
      <Button.Group className='positive--blue'>
        <Button>Iphone</Button>
        <Button.Or />
        <Button positive>Ipad</Button>
      </Button.Group>
    )
  }
}
